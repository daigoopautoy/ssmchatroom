package com.ss.pojo;

import java.util.List;

public class Administrator {
	private Long administratorID;
	private Long administratorPassword;
	private Integer level;
	private List<User> userList;

	public Administrator() {
	}

	public Administrator(Long administratorID, Long administratorPassword, Integer level, List<User> userList) {
		this.administratorID = administratorID;
		this.administratorPassword = administratorPassword;
		this.level = level;
		this.userList = userList;
	}

	public Long getAdministratorID() {
		return administratorID;
	}

	public void setAdministratorID(Long administratorID) {
		this.administratorID = administratorID;
	}

	public Long getAdministratorPassword() {
		return administratorPassword;
	}

	public void setAdministratorPassword(Long administratorPassword) {
		this.administratorPassword = administratorPassword;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	@Override
	public String toString() {
		return "Administrator{" +
				"administratorID=" + administratorID +
				", administratorPassword=" + administratorPassword +
				", level=" + level +
				", userList=" + userList +
				'}';
	}
}
