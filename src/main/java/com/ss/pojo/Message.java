package com.ss.pojo;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {
	private String type;
	private String content;
	private String userID;
	private String toID;
	private Date time;

	public Message() {
	}

	public Message(String type, String content, String userID, String toID, Date time) {
		this.type = type;
		this.content = content;
		this.userID = userID;
		this.toID = toID;
		this.time = time;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getToID() {
		return toID;
	}

	public void setToID(String toID) {
		this.toID = toID;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}


	@Override
	public String toString() {
		return "Message{" +
				"type=" + type +
				", content='" + content + '\'' +
				", userID=" + userID +
				", toID=" + toID +
				", time=" + time +
				'}';
	}
}
