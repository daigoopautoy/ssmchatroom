package com.ss.pojo;

import java.io.File;
import java.util.List;

public class PrivateChatObject implements ChatObject {
	private Long privateChatObjectID;
	private File privateChatObjectAvatar;
	private String privateChatObjectNickname;
	private List<Message> privateChatObject;

	public PrivateChatObject() {
	}

	public PrivateChatObject(Long privateChatObjectID, File privateChatObjectAvatar, String privateChatObjectNickname, List<Message> privateChatObject) {
		this.privateChatObjectID = privateChatObjectID;
		this.privateChatObjectAvatar = privateChatObjectAvatar;
		this.privateChatObjectNickname = privateChatObjectNickname;
		this.privateChatObject = privateChatObject;
	}

	public Long getPrivateChatObjectID() {
		return privateChatObjectID;
	}

	public void setPrivateChatObjectID(Long privateChatObjectID) {
		this.privateChatObjectID = privateChatObjectID;
	}

	public File getPrivateChatObjectAvatar() {
		return privateChatObjectAvatar;
	}

	public void setPrivateChatObjectAvatar(File privateChatObjectAvatar) {
		this.privateChatObjectAvatar = privateChatObjectAvatar;
	}

	public String getPrivateChatObjectNickname() {
		return privateChatObjectNickname;
	}

	public void setPrivateChatObjectNickname(String privateChatObjectNickname) {
		this.privateChatObjectNickname = privateChatObjectNickname;
	}

	public List<Message> getPrivateChatObject() {
		return privateChatObject;
	}

	public void setPrivateChatObject(List<Message> privateChatObject) {
		this.privateChatObject = privateChatObject;
	}

	@Override
	public String toString() {
		return "PrivateChatObject{" +
				"privateChatObjectID=" + privateChatObjectID +
				", privateChatObjectAvatar=" + privateChatObjectAvatar +
				", privateChatObjectNickname='" + privateChatObjectNickname + '\'' +
				", privateChatObject=" + privateChatObject +
				'}';
	}
}
