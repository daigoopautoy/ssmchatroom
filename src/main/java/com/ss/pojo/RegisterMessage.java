package com.ss.pojo;

public class RegisterMessage {
	public static final Integer USER_ID_EXIST = 1; //注册名已经存在
	public static final Integer USER_ID_OR_PASSWORD_NULL = 2;
	private boolean succeeded;  //是否注册成功
	private Integer errCause;  //错误原因

	public RegisterMessage() {
	}

	public RegisterMessage(boolean succeeded) {
		this.succeeded = succeeded;
	}

	public RegisterMessage(Integer errCause) {
		this.succeeded = false;
		this.errCause = errCause;
	}

	public boolean isSucceeded() {
		return succeeded;
	}

	public void setSucceeded(boolean succeeded) {
		this.succeeded = succeeded;
	}

	public Integer getErrCause() {
		return errCause;
	}

	public void setErrCause(Integer errCause) {
		this.errCause = errCause;
	}
}
