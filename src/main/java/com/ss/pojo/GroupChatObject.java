package com.ss.pojo;

import java.io.File;
import java.util.List;

public class GroupChatObject implements ChatObject {
	private Long groupChatObjectID;
	private File groupChatObjectAvatar;
	private String groupChatObjectNickname;
	private List<Message> groupChatObject;

	public GroupChatObject() {
	}

	public GroupChatObject(Long groupChatObjectID, File groupChatObjectAvatar, String groupChatObjectNickname, List<Message> groupChatObject) {
		this.groupChatObjectID = groupChatObjectID;
		this.groupChatObjectAvatar = groupChatObjectAvatar;
		this.groupChatObjectNickname = groupChatObjectNickname;
		this.groupChatObject = groupChatObject;
	}

	public Long getGroupChatObjectID() {
		return groupChatObjectID;
	}

	public void setGroupChatObjectID(Long groupChatObjectID) {
		this.groupChatObjectID = groupChatObjectID;
	}

	public File getGroupChatObjectAvatar() {
		return groupChatObjectAvatar;
	}

	public void setGroupChatObjectAvatar(File groupChatObjectAvatar) {
		this.groupChatObjectAvatar = groupChatObjectAvatar;
	}

	public String getGroupChatObjectNickname() {
		return groupChatObjectNickname;
	}

	public void setGroupChatObjectNickname(String groupChatObjectNickname) {
		this.groupChatObjectNickname = groupChatObjectNickname;
	}

	public List<Message> getGroupChatObject() {
		return groupChatObject;
	}

	public void setGroupChatObject(List<Message> groupChatObject) {
		this.groupChatObject = groupChatObject;
	}

	@Override
	public String toString() {
		return "GroupChatObject{" +
				"groupChatObjectID=" + groupChatObjectID +
				", groupChatObjectAvatar=" + groupChatObjectAvatar +
				", groupChatObjectNickname='" + groupChatObjectNickname + '\'' +
				", groupChatObject=" + groupChatObject +
				'}';
	}
}
