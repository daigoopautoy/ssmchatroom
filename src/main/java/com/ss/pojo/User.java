package com.ss.pojo;

import com.fasterxml.jackson.annotation.JsonAlias;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	@JsonAlias(value = {"userID","username","name"})
	private String userID;
	@JsonAlias(value = {"userPassword","password"})
	private String userPassword;
	private Date loginDate;
	private String nickname;
	private Integer state;
	private String signature;
	private String userAvatar;
	private Integer chatlistID;

	public User() {
	}

	public User(String userID, String userPassword, String nickname, Integer state, String signature, String userAvatar, Date loginDate, Integer chatlistID) {
		this.userID = userID;
		this.userPassword = userPassword;
		this.nickname = nickname;
		this.state = state;
		this.signature = signature;
		this.userAvatar = userAvatar;
		this.loginDate = loginDate;
		this.chatlistID = chatlistID;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getUserAvatar() {
		return userAvatar;
	}

	public void setUserAvatar(String userAvatar) {
		this.userAvatar = userAvatar;
	}

	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public Integer getChatlistID() {
		return chatlistID;
	}

	public void setChatlistID(Integer chatlistID) {
		this.chatlistID = chatlistID;
	}

	/*public List<ChatObject> getChatlistID() {
		return chatlistID;
	}

	public void setChatlistID(List<ChatObject> chatlistID) {
		this.chatlistID = chatlistID;
	}*/

	@Override
	public String toString() {
		return "User{" +
				"userID='" + userID + '\'' +
				", userPassword='" + userPassword + '\'' +
				", nickname='" + nickname + '\'' +
				", state=" + state +
				", signature='" + signature + '\'' +
				", userAvatar=" + userAvatar +
				", loginDate=" + loginDate +
				", chatlistID=" + chatlistID +
				'}';
	}
}
