package com.ss.pojo;

/**
 * 传递给前端的消息类型
 */
public class LoginMessage {
	public static final Integer USER_ID_NOT_EXIST = 1; //当前登录的用户名尚未注册
	public static final Integer USER_PASSWORD_WRONG = 2; //密码错误
	public static final Integer USER_ID_OR_PASSWORD_NULL = 3; //用户姓名或密码不合规范
	private boolean succeeded; //登录是否成功
	private Integer errCause; //错误原因

	public LoginMessage() {
	}

	public LoginMessage(boolean succeeded) {
		this.succeeded = succeeded;
	}

	public LoginMessage(Integer errCause) {
		this.succeeded = false;
		this.errCause = errCause;
	}

	public boolean isSucceeded() {
		return succeeded;
	}

	public void setSucceeded(boolean succeeded) {
		this.succeeded = succeeded;
	}

	public Integer getErrCause() {
		return errCause;
	}

	public void setErrCause(Integer errCause) {
		this.errCause = errCause;
	}
}
