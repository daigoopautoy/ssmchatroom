package com.ss.web.websocket.stomp;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
@Configuration
//启用STOMP消息
@EnableWebSocketMessageBroker
public class WebSocketStompConfig extends AbstractWebSocketMessageBrokerConfigurer {
	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
//		客户端在订阅或发布消息到目的地路径前，要连接该端点
		//registry.addEndpoint("/websocket");
		registry.addEndpoint("/websocket").withSockJS();

	}

	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
//		如果不重载，则默认处理前缀“/topic”的消息
//		添加可以向客户端发消息的域
		registry.enableSimpleBroker("/topic", "/queue", "/users");
//		以应用程序为目的地的消息会转到@MessageMapping方法中
//		表示客户端向服务器发送时需要加/app
		registry.setApplicationDestinationPrefixes("/app");
//		添加给指定用户发送的前缀
		registry.setUserDestinationPrefix("/users/");
	}

}
