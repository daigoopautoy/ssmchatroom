package com.ss.web;

import com.ss.pojo.LoginMessage;
import com.ss.pojo.RegisterMessage;
import com.ss.pojo.User;
import com.ss.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;


@Controller
public class LoginController {
	//	日志
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;

	@Autowired
	private UserService userService;



	private static final String SUBSCRIBE_LOGIN_URI = "/topic/login"; //订阅的登录地址



	@RequestMapping(value = "/reply/login",method = RequestMethod.POST)
	@ResponseBody
	public LoginMessage replyLoginMessage(@RequestBody User user){
		/*System.out.println("按下登录按钮");
		System.out.println(user.getUserID()+","+user.getUserPassword());*/
//		应加上不能重复登录
		if (user.getUserID() == null || user.getUserID().trim().equals("")){
			return new LoginMessage(LoginMessage.USER_ID_OR_PASSWORD_NULL);
		}
		User user1 = userService.queryUserByID(user.getUserID());
		if (user1 == null) {
			return new LoginMessage(LoginMessage.USER_ID_NOT_EXIST);
		}
		user1=userService.validateUser(user.getUserID(), user.getUserPassword());
		if (user1 == null) {
			return new LoginMessage(LoginMessage.USER_PASSWORD_WRONG);
		}
//		1.检查账号密码是否为空，前端工作
//		2.service包返回查询账号结果，不存在直接返回失败消息
//		3.service包返回查询密码结果，不对直接返回失败消息
//		4.返回成功消息
		return new LoginMessage(true);
	}

	@RequestMapping(value = "/reply/regist",method = RequestMethod.POST)
	@ResponseBody
	public RegisterMessage replyRegisterMessage(@RequestBody User user) {
		System.out.println("按下注册按钮");
		System.out.println(user.getUserID());
		if (user.getUserID() == null || user.getUserID().trim().equals("")){
			return new RegisterMessage(RegisterMessage.USER_ID_OR_PASSWORD_NULL);
		}
		User user1 = userService.queryUserByID(user.getUserID());
		if (user1 != null) {
			return new RegisterMessage(RegisterMessage.USER_ID_EXIST);
		}
		if (user.getUserPassword() != null) {
			userService.register(user.getUserID(),user.getUserPassword());
		}
		user1 = userService.queryUserByID(user.getUserID());
		System.out.println(user1);
//		1.检查账号密码是否为空，前端工作
//		2.service包返回查询账号结果，已存在直接返回失败消息
//		3.service包插入新用户
//		4.返回成功消息
		return new RegisterMessage(true);
	}

	@RequestMapping(value = "/chat", method = RequestMethod.POST)
	public String loginIntoChatRoom(User user, HttpServletRequest request) {
		/*System.out.println("进入聊天主界面");*/
//		1.判断user合法性
		if (user == null||userService.isOnline(user.getUserID())) {
//			重返登录界面
			return "index";
		}
		user.setLoginDate(new Date());
		user.setUserPassword(null);//防止泄露
//		2.记录登录日期，清除密码信息
		HttpSession session=request.getSession();
		session.setAttribute("user", user);
//		3.保存登录信息
		userService.addLoginUser(user.getUserID(),user.getLoginDate());
//		4.用hashmap记录在线人数
//		5.传到订阅的登录地址
		/*System.out.println(user.getUserID()+","+user.getLoginDate());*/
		simpMessagingTemplate.convertAndSend(SUBSCRIBE_LOGIN_URI,user);
		logger.info(user.getUserID()+" login at "+user.getLoginDate());
		return "chatroom";

	}

	@RequestMapping(value = {"/", "/index", ""}, method = RequestMethod.GET)
	public String index() {
		return "index";
	}

	@SubscribeMapping("/chat/participants")
	public Long getActiveUserNumber(){
//		用hashmap传值
		return Long.valueOf(userService.getOnlineUsersNumber());
	}


}
