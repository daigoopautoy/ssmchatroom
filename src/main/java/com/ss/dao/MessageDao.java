package com.ss.dao;

import org.apache.ibatis.annotations.Param;

import java.util.Date;

public interface MessageDao {
	void record(@Param("userID") String userID,
	            @Param("date") Date date,
	            @Param("content") String content,
	            @Param("type") String type);
}
