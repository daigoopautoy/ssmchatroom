package com.ss.dao;

import com.ss.pojo.User;
import org.apache.ibatis.annotations.Param;

public interface UserDao {
	User queryUserByID(@Param("userID")String userID);

	User queryUserByIDAndPassword(@Param("userID") String userID, @Param("userPassword") String userPassword);

	void addUser(@Param("userID") String userID, @Param("userPassword") String userPassword);

	void modifyUserPassword(@Param("userID") String userID,@Param("userPassword")String userPassword,@Param("newPassword")String newPassword);

	void deleteUser(@Param("userID") String userID);

}
