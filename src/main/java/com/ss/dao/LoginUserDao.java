package com.ss.dao;

import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface LoginUserDao {

	Integer getOnlineUsersNumber();

	void addLoginUser(@Param("userID")String userID,@Param("date")Date date);

	Integer queryOnlineUser(@Param("userID") String userID);

	List<String> getOnlineUserList();
}
