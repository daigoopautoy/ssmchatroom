package com.ss.service.impl;

import com.ss.dao.MessageDao;
import com.ss.pojo.Message;
import com.ss.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
@Service
public class MessageServiceImpl implements MessageService {
	@Autowired
	private MessageDao messageDao;
	@Override
	public void record(String userID, Date date, String content, String type) {
		messageDao.record(userID,date,content,type);
	}

	@Override
	public void recordMessage(Message message) {
		messageDao.record(message.getUserID(),message.getTime(),message.getContent(),message.getType());
	}
}
