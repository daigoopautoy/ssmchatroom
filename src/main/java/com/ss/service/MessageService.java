package com.ss.service;

import com.ss.pojo.Message;

import java.util.Date;

public interface MessageService {
	public void record(String userID, Date date, String content, String type);

	public void recordMessage(Message message);
}
