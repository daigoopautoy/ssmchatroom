package com.ss.service;

import com.ss.pojo.User;

import java.util.Date;
import java.util.List;

public interface UserService {

	public User validateUser(String userID, String userPassword);

	public boolean isExistUser(String userID);

	public void register(String userID, String userPassword);

	public User queryUserByID(String userID);

	public Integer getOnlineUsersNumber();

	public void addLoginUser(String userID, Date date);

	public boolean isOnline(String userID);
}
