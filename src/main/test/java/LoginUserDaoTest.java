import com.ss.dao.LoginUserDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.Iterator;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/spring-dao.xml"})

public class LoginUserDaoTest {
	@Autowired
	private LoginUserDao loginUserDao;
	@Test
	public void testGetOnlineUsersNumber() {
		System.out.println(loginUserDao.getOnlineUsersNumber());
	}

	@Test
	public void testAddLoginUser() {
		loginUserDao.addLoginUser("456", new Date());
	}

	@Test
	public void testQueryOnlineUser() {
		String userID = "456";
		System.out.println(loginUserDao.queryOnlineUser(userID));
	}

	@Test
	public void testGetOnlineUserList() {
		Iterator<String> iterator=loginUserDao.getOnlineUserList().iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
}
