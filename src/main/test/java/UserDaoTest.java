import com.ss.dao.UserDao;
import com.ss.pojo.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.user.UserRegistryMessageHandler;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/spring-dao.xml"})


public class UserDaoTest {
	@Autowired
	private UserDao userDao;
	@Test
	public void testQueryUserByID() throws Exception {
		String userID = "456";
		User user =userDao.queryUserByID(userID);
		System.out.println(user);
	}
	@Test
	public void testQueryUserByIDAndPassword(){
		String userID = "456";
		String userPassword = "789";
		User user = userDao.queryUserByIDAndPassword(userID, userPassword);
		System.out.println(user);
	}

	@Test
	public void testAddUser() {
		String userID = "789";
		String userPassword = "123";
		userDao.addUser(userID,userPassword);
	}

	@Test
	public void testModifyUserPassword() {
		String userID = "789";
		String userPassword = "123";
		String newPassword = "789";
		userDao.modifyUserPassword(userID,userPassword,newPassword);
	}

	@Test
	public void testDeleteUser() {
		String userID = "789";
		userDao.deleteUser(userID);
	}
}
